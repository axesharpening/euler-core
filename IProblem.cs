using System;

namespace euler_core
{

    public interface IProblem
    {
        string URL{get;set;}
        string Title{get;set;}
        string Description{get;set;}

        T Solve<T>();
    }

}