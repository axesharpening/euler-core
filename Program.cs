﻿using System;

namespace euler_core
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Start solving Euler Project");

            foreach(var p in Helpers.ProblemList)
            {
                try
                {
                    Console.WriteLine("======");
                    Console.WriteLine("Title: {0}", p.Title);
                    Console.WriteLine("==");
                    Console.WriteLine("Description: {0}", p.Description);
                    Console.WriteLine("==");
                    
                    var Result = p.Solve<object>();
                    Console.WriteLine($"Result: {Result}");
 
                }
                catch(Exception ex)
                {
                    Console.WriteLine("===ERROR===");
                    Console.WriteLine(ex.Message);
                    Console.WriteLine("===ERROR Trace===");
                    Console.WriteLine(ex.ToString());
                }
                finally
                {
                    Console.WriteLine("============================");
                }
 
            }
 
            Console.WriteLine("The End!!");
            Console.WriteLine("<ENTER> to quit");
            Console.ReadKey();
        }
    }
}
