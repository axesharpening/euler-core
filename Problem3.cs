using System;

namespace euler_core
{
    public class Problem3:IProblem
    {
        
        public string URL{get; set;} = @"https://projecteuler.net/problem=3";
        public string Title{get;set;} = "Problem 3: Largest prime factor";
        public string Description{get; set;} = @"The prime factors of 13195 are 5, 7, 13 and 29.
                                                    What is the largest prime factor of the number 600851475143 ?";
        
        public T Solve<T>(){

            long Result= 0; //The lagest Prime Factor
            const long prResult = 600851475143;
            long prRRolling = 1;

            //A prime number is a natural number greater than 1 that cannot be formed by multiplying two smaller natural numbers.
            //13195 = 5*7*13*29
            

            // for (long i = 2; i < prResult; i++) {
            //     if (prResult % i == 0) { // Check the Number is a divisor
            //         bool isPrime = true;
            //         for (long j = 2; j < i; j++) {
            //             if (i % j == 0) {
            //                 isPrime = false;
            //                 break;
            //             }
            //         }
            //         if (isPrime) {
            //             Result = i;
            //         }
            //     }
            // }
 
            return (T) Convert.ChangeType(Result, typeof(T));
        }

    }
    
}