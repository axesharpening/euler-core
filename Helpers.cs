using System;
using System.Collections.Generic;

namespace euler_core
{
    public static class Helpers
    {
        public static List<IProblem> ProblemList {
             get
                {

                    var problemList = new List<IProblem>(){  new Problem1()  , new Problem2(), new Problem3()  };

                    return problemList;
                }
        } 
    }
}   