using System;

 
namespace euler_core
{

    public class Problem1 : IProblem{
        public string URL{get; set;} = @"https://projecteuler.net/problem=1";
        public string Title{get;set;} = "Problem 1: Multiples of 3 and 5";

        public string Description{get; set;} = @"If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.
                                                    Find the sum of all the multiples of 3 or 5 below 1000.";

        public T Solve<T>(){

            int iResult= 0;

            //Maybe not the most efficiant but it works
            for(var i=1;i<1000;i++)
            {
                //Check the Mod(%) for a remander and when it is Zero add the number onto the Result
                if((i%3)==0 || (i%5)==0){
                    iResult += i; //Increase the result
                }
            }

            return (T) Convert.ChangeType(iResult, typeof(T));
        }

    }

}